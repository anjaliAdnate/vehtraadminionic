export class URLS {
    mainURL: string = "http://www.vehtraschools.com";
    usersURL: string = "http://www.vehtraschools.com/users/";
    devicesURL: string = "http://www.vehtraschools.com/devices";
    gpsURL: string = "http://www.vehtraschools.com/gps";
    geofencingURL: string = "http://www.vehtraschools.com/geofencing";
    trackRouteURL: string = "http://www.vehtraschools.com/trackRoute";
    groupURL: string = "http://www.vehtraschools.com/groups/";
    notifsURL: string = "http://www.vehtraschools.com/notifs";
    stoppageURL: string = "http://www.vehtraschools.com/stoppage";
    summaryURL: string = "http://www.vehtraschools.com/summary";
    shareURL: string = "http://www.vehtraschools.com/share";
    usertripURL: string = "http://www.vehtraschools.com/user_trip/";
    notifioURL: string = "http://www.vehtraschools.com/notifIO";
    dev_url: string = "http://www.vehtraschools.com";
    poi_url: string = "http://www.vehtraschools.com/poi";
}