import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, IonicPage, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as moment from 'moment';
import * as _ from "lodash";
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsMapTypeId, GoogleMapsEvent, Polyline } from '@ionic-native/google-maps';
import { URLS } from '../../providers/urls';
declare var google: any;


@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live-single-device.html',
})
export class LiveSingleDevice implements OnInit, OnDestroy {

  drawerHidden = true;
  drawerHidden1 = false;
  shouldBounce = true;
  dockedHeight = 150;
  bounceThreshold = 500;
  distanceTop = 56;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  selectedFlag: any = { _id: '', flag: false };
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  showActionSheet: boolean;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any = 0;
  vehicle_speed: any = 0;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  isEnabled: boolean = false;
  liveDataShare: any;
  showShareBtn: boolean = false;
  resToken: any;
  mapData: any[];
  mapData1: any[];
  menuActive: boolean;
  motionActivity: string;
  mapHideTraffic: boolean = false;
  locateme: boolean = false;
  latlngCenter: any;
  last_ping_on: any;
  deviceDeatils: any = {};
  trackRouteID: any;
  locations: any[];
  routeData: any;
  tempArray: any[];
  colorObj: any = "gpsc";
  // polyLines: Polyline;
  polyLines: any[] = [];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    public modalCtrl: ModalController,
    private socialSharing: SocialSharing,
    public plt: Platform,
    public urls: URLS
  ) {
    // document.body.style.transform = 'rotateZ(0deg)';
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};

    if (navParams.get("device") != null) {
      var stringify = JSON.stringify(navParams.get("device"))
      this.deviceDeatils = JSON.parse(stringify);
      if(localStorage.getItem("liveDeviceData") != null ){
        localStorage.removeItem("liveDeviceData");
      }
      localStorage.setItem("liveDeviceData", stringify);
      console.log("device id => ", this.deviceDeatils)
    }

    // UI members.
    this.motionActivity = 'Activity';
    this.menuActive = false;

  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [10, 20], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = {
    "car": this.carIcon,
    "bike": this.carIcon,
    "truck": this.carIcon,
    "bus": this.carIcon,
    "user": this.carIcon
  }

  back() {
    this.navCtrl.pop();
  }

  showRoute() {
    if (this.colorObj == 'gpsc') {
      if (this.deviceDeatils) {
        this.polyLines = [];
        this.allData.markmark = [];
        if (this.deviceDeatils.scheduleschools.length > 0) {
          for (var w = 0; w < this.deviceDeatils.scheduleschools.length; w++) {
            this.trackRouteID = this.deviceDeatils.scheduleschools[w].trackRoute;
            this.getTrackAPI(this.trackRouteID);
          }
        }
      }
      this.colorObj = 'light';
    } else {
      if (this.colorObj == 'light') {
        for (var i = 0; i < this.polyLines.length; i++) {
          this.polyLines[i].remove();
        }
        for (var r = 0; r < this.allData.markmark.length; r++) {
          this.allData.markmark[r].remove();
        }
        this.colorObj = 'gpsc';
      }
    }
  }

  getTrackAPI(trackid) {

    this.apiCall.getTrackRoute(trackid, this.userdetails._id)
      .subscribe(resp => {
        let that = this;
        (function (data) {
          if (data == undefined) {
            return;
          }
          that.routeData = data;
          that.mapData1 = [];
          for (var i = 0; i < that.routeData.routePath.length; i++) {
            that.mapData1.push({ "lat": that.routeData.routePath[i].location.coordinates[1], "lng": that.routeData.routePath[i].location.coordinates[0] });
          }
          that.locations = [];
          for (var i = 0; i < data.poi.length; i++) {
            if (data.poi[i].functionality == 'STOP') {
              var ampm = (data.poi[i].H >= 12) ? "PM" : "AM";
              var hourss = new Date().getHours();
              var hour, min;
              if ((data.poi[i].H).toString().length < 2) {
                hour = "0" + (data.poi[i].H).toString();
              } else {
                hour = (data.poi[i].H).toString();
              }
              if ((data.poi[i].M).toString().length < 2) {
                min = "0" + (data.poi[i].M).toString();
              } else {
                min = (data.poi[i].M).toString();

              }
              var arr = {
                'lat': data.poi[i].poi.location.coordinates[1],
                'lng': data.poi[i].poi.location.coordinates[0],
                'stopName': data.poi[i].poi.poiname,
                'sbUsers': data.poi[i].sbUsers[0],
                'timestamp': hour + ':' + min + ' ' + ampm,
                'routeIndex': data.poi[i].poi.routeIndex
              };
              if (hourss >= 12 && data.poi[i].H >= 12) {
                that.locations.push(arr);
              } else {
                if (hourss < 12 && data.poi[i].H < 12) {
                  that.locations.push(arr);
                }
              }
            }
          }
          var iconurl;
          var parkicon;
          if (that.plt.is('ios')) {
            iconurl = 'www/assets/imgs/flag.png';
            parkicon = 'www/assets/imgs/park.png';
          } else {
            if (that.plt.is('android')) {
              iconurl = './assets/imgs/flag.png';
              parkicon = './assets/imgs/park.png';
            } else {
              iconurl = './assets/imgs/flag.png';
              parkicon = './assets/imgs/park.png';
            }
          }

          for (var i = 0; i < that.locations.length; i++) {

            // var infowindow = new google.maps.InfoWindow({});
            if (that.locations[i].sbUsers = that.userdetails._id) {
              that.allData.map.addMarker({
                title: that.locations[i].routeIndex + '. ' + that.locations[i].stopName + ' - ' + that.locations[i].timestamp,
                position: that.locations[i],
                icon: iconurl,
                labels: that.locations[i].routeIndex
              }).then((marker: Marker) => {
                that.allData.markmark.push(marker);

                if (that.allData.markmark.length == that.locations.length) {
                  for (var m = 0; m < that.allData.markmark.length; m++) {
                    // console.log("marker array in allData: ", m, ' ', that.allData.markmark[m])
                    that.allData.markmark[m].showInfoWindow();
                  }
                }
              });
            } else {
              that.allData.map.addMarker({
                title: that.locations[i].stopName,
                position: that.locations[i],
                icon: parkicon
              }).then((marker: Marker) => {
                marker.showInfoWindow();
              });
            }
          }
          // List<Polyline> polylines = new ArrayList<Polyline>();
          that.allData.map.addPolyline({
            points: that.mapData1,
            color: '#42adf4',
            width: 5,
            geodesic: true
          }).then((poly: Polyline) => {
            that.polyLines.push(poly);
          })

        })(resp)

      },
        err => {
          console.log("we got error: ", err)
        })
  }

  newMap() {
    let mapOptions = {
      camera: { zoom: 10 },
      gestures: {
        rotate: false,
        tilt: false
      }
    };
    let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
    return map;
  }

  ngOnInit() {
    this._io = io.connect(this.urls.mainURL + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    this.showShareBtn = true;
    var paramData = this.navParams.get("device");
    this.titleText = paramData.Device_Name;

    this.temp(paramData);
    this.showActionSheet = true;
  }

  ngOnDestroy() {
    let that = this;
    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    that._io.on('disconnect', () => {
      that._io.open();
    })
    localStorage.removeItem("liveDeviceData")
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }

  trafficFunc() {
    let that = this;
    that.isEnabled = !that.isEnabled;
    if (that.isEnabled == true) {
      that.allData.map.setTrafficEnabled(true);
    } else {
      that.allData.map.setTrafficEnabled(false);
    }
  }

  shareLive() {
    var data = {
      id: this.liveDataShare._id,
      imei: this.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: 60   // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    let that = this;
    var link = this.urls.mainURL + "/share/liveShare?t=" + that.resToken;
    that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;

    return h + ':' + m + ':' + s;
  }

  settings() {
    var paramData = this.navParams.get("device");
    this.temp(paramData);
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: this.deviceDeatils
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      var paramData = this.navParams.get("device");
      this.temp(paramData);
    })
  }

  socketInit(pdata) {
    // debugger
    let that = this;

    let key = pdata._id;
    if (that.allData.refmarker[key] != undefined) {
      that.allData.refmarker[key].remove();
    }
    // if (that.allData.marker[key] == undefined) {

    //   that.vehicle_speed = pdata.last_speed;
    //   that.todays_odo = pdata.today_odo;
    //   that.total_odo = pdata.total_odo;
    //   that.last_ping_on = pdata.last_ping_on;
    //   if (pdata && pdata.last_location && pdata.last_location.lat && pdata.last_location.long) {
    //     let ddd: any = { lat: pdata.last_location.lat, lng: pdata.last_location.long }
    //     let icon = "";
    //     if (moment().diff(moment(pdata.last_ping_on), 'minutes') < 60) {
    //       if (that.plt.is('ios')) {
    //         icon = 'www/assets/imgs/vehicles/runningbus.png';
    //       } else {
    //         if (that.plt.is('android')) {
    //           icon = './assets/imgs/vehicles/runningbus.png';
    //         } else {
    //           icon = './assets/imgs/vehicles/runningbus.png';
    //         }
    //       }
    //     }
    //     else {
    //       if (that.plt.is('ios')) {
    //         icon = 'www/assets/imgs/vehicles/stoppedbus.png';
    //       } else {
    //         if (that.plt.is('android')) {
    //           icon = './assets/imgs/vehicles/stoppedbus.png';
    //         } else {
    //           icon = './assets/imgs/vehicles/stoppedbus.png';
    //         }
    //       }
    //     }
    //     that.allData.map.addMarker({
    //       position: ddd,
    //       title: pdata.Device_Name,
    //       icon: icon
    //     }).then((marker: Marker) => {
    //       // that.allData.marker[pdata._id] = marker;
    //       that.allData.refmarker[pdata._id] = marker;
    //       // that.allData.marker[pdata._id].dicon = icon;
    //       console.log("marker added here: ")
    //     })
    //   }
    // } else {
    //   that.allData.marker[key].remove();
    // }
    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 == null) return;

      let dd1: any = { lat: d4.last_location.lat, lng: d4.last_location.long };
      let dd2: any = { lat: d4.sec_last_location.lat, lng: d4.sec_last_location.long };
      debugger
      console.log("merker check id: ", d4._id)
      if (that.allData.marker[d4._id] == undefined || that.allData.marker[d4._id] == null) {

        if (that.allData.refmarker[d4._id] != undefined)
          that.allData.refmarker[d4._id].remove();

        var icon1;
        if (that.plt.is('ios')) {
          icon1 = 'www/assets/imgs/vehicles/runningbus.png';
        } else {
          if (that.plt.is('android')) {
            icon1 = './assets/imgs/vehicles/runningbus.png';
          } else {
            icon1 = './assets/imgs/vehicles/runningbus.png';
          }
        }

        that.allData.map.addMarker({
          position: dd1,
          title: d4.Device_Name,
          icon: icon1
        }).then((marker: Marker) => {
          debugger
          that.allData.marker[d4._id] = marker;
          that.allData.marker[d4._id].dicon = icon1;
          console.log("second marker added here: ")
        })
      } else {
        let icon = "";
        if (moment().diff(moment(d4.last_ping_on), 'minutes') < 60) {
          if (that.plt.is('ios')) {
            that.allData.marker[d4._id].icon = 'www/assets/imgs/vehicles/runningbus.png';
          } else {
            if (that.plt.is('android')) {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/runningbus.png';
            } else {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/runningbus.png';
            }
          }
        } else {
          if (that.plt.is('ios')) {
            that.allData.marker[d4._id].icon = 'www/assets/imgs/vehicles/stoppedbus.png';
          } else {
            if (that.plt.is('android')) {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/stoppedbus.png';
            } else {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/stoppedbus.png';
            }
          }
        }

        debugger
        that.vehicle_speed = d4.last_speed;
        that.todays_odo = d4.today_odo;
        that.total_odo = d4.total_odo;
        that.last_ping_on = d4.last_ping_on;

        that.allData.marker[d4._id].setIcon(that.allData.marker[d4._id].dicon);
        if (that.ongoingMoveMarker[d4._id])
          clearTimeout(that.ongoingMoveMarker[d4._id]);
        if (that.ongoingGoToPoint[d4._id])
          clearTimeout(that.ongoingGoToPoint[d4._id]);
        that.allData.marker[d4._id].setPosition(dd2);
        that.liveTrack(that.allData.map, that.allData.marker[d4._id], [dd1], 50, 10, d4._id)
      }
    })
  }
  liveTrack(map, mark, coords, speed, delay, id) {
    let that = this;
    var target = 0;

    // clearTimeout(that.ongoingGoToPoint[id]);
    // clearTimeout(that.ongoingMoveMarker[id]);

    // if (center) {
    //   map.setCameraTarget(coords[0]);
    // }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          map.setCameraTarget(new LatLng(lat, lng));
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          map.setCameraTarget(dest);
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  zoomin() {
    let that = this;
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  temp(data) {

    let that = this;
    that.showShareBtn = true;

    that.liveDataShare = data;

    that.drawerHidden = true;
    that.onClickShow = false;

    if (that.allData.map != undefined) {
      that.allData.map.remove();
    }

    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    that.allData = {};
    that.socketChnl = [];
    that.socketSwitch = {};
    let styles = [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ebe3cd"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#523735"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f1e6"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#c9b2a6"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#dcd2be"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#ae9e90"
          }
        ]
      },
      {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#93817c"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#a5b076"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#447530"
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffeb3b"
          },
          {
            "weight": 1
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "labels.icon",
        "stylers": [
          {
            "color": "#ff6841"
          },
          {
            "weight": 8
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#ff6841"
          },
          {
            "weight": 1
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#fff9fb"
          },
          {
            "weight": 8
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f1e6"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#fdfcf8"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f8c967"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#e9bc62"
          }
        ]
      },
      {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e98d58"
          }
        ]
      },
      {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#db8555"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#806b63"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#8f7d77"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#ebe3cd"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#b9d3c2"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#92998d"
          }
        ]
      }
    ];

    if (data) {
      if (data.last_location) {
        let mapOptions = {
          backgroundColor: 'white',
          controls: {
            compass: true,
            zoom: false,
            mapToolbar: true
          },
          gestures: {
            rotate: false,
            tilt: false
          },
          styles: styles
        }
        let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
        map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
          document.body.style.transform = 'rotateZ(0deg)';
          // map.animateCamera({
          //   target: { lat: 20.5937, lng: 78.9629 },
          //   zoom: 18,
          //   duration: 3000,
          //   padding: 0  // default = 20px
          // });

          // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          // that.allData.map = map;
          // that.allData.marker = [];
          // that.socketInit(data);
        });

        map.animateCamera({
          target: { lat: 20.5937, lng: 78.9629 },
          zoom: 18,
          duration: 3000,
          padding: 0  // default = 20px
        });

        map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
        that.allData.map = map;
        that.allData.marker = [];
        that.allData.refmarker = [];
        debugger
        if (that.allData.marker[data._id] == undefined) {
          that.vehicle_speed = data.last_speed;
          that.todays_odo = data.today_odo;
          that.total_odo = data.total_odo;
          that.last_ping_on = data.last_ping_on;
          if (data && data.last_location && data.last_location.lat && data.last_location.long) {
            let ddd: any = { lat: data.last_location.lat, lng: data.last_location.long }
            let icon = "";
            if (moment().diff(moment(data.last_ping_on), 'minutes') < 60) {
              if (that.plt.is('ios')) {
                icon = 'www/assets/imgs/vehicles/runningbus.png';
              } else {
                if (that.plt.is('android')) {
                  icon = './assets/imgs/vehicles/runningbus.png';
                } else {
                  icon = './assets/imgs/vehicles/runningbus.png';
                }
              }
            }
            else {
              if (that.plt.is('ios')) {
                icon = 'www/assets/imgs/vehicles/stoppedbus.png';
              } else {
                if (that.plt.is('android')) {
                  icon = './assets/imgs/vehicles/stoppedbus.png';
                } else {
                  icon = './assets/imgs/vehicles/stoppedbus.png';
                }
              }
            }
            that.allData.map.addMarker({
              position: ddd,
              title: data.Device_Name,
              icon: icon
            }).then((marker: Marker) => {
              that.allData.refmarker[data._id] = marker;
              console.log("marker added here: ")
              that.socketInit(data);
            })
          }
        }

      } else {
        that.allData.map = that.newMap();
        that.socketInit(data);
      }
    }
  }


  info(mark, cb) {
    mark.addListener('click', cb);
  }

  show(data, key) {
    if (data != undefined) {
      if (key == 'last_ping_on') {
        return moment(data[key]).format('DD/MM/YYYY, h:mm:ss a');
      }
      else {
        return data[key];
      }

    }
  }
  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
    // this.playSound((this.menuActive) ? 'OPEN' : 'CLOSE');
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      // this.locateme = !this.locateme;
      // if(this.locateme) {
      that.allData.map.setCameraTarget(that.latlngCenter);
      // }
    } else {
      if (type == 'mapHideTraffic') {
        this.mapHideTraffic = !this.mapHideTraffic;
        console.log(this.mapHideTraffic)
        if (this.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
        } else {
          that.allData.map.setTrafficEnabled(false);
        }
      }
    }

  }

  reloadmap() {
    if (this.polyLines.length > 0) {
      for (var i = 0; i < this.polyLines.length; i++) {
        this.polyLines[i].remove();
      }
      for (var r = 0; r < this.allData.markmark.length; r++) {
        this.allData.markmark[r].remove();
      }
      this.colorObj = 'gpsc';
    }
    if (localStorage.getItem("liveDeviceData") != null) {
      let that = this;
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);
      that._io.on('disconnect', () => {
        that._io.open();
      })
      var stringify = JSON.parse(localStorage.getItem("liveDeviceData"))
      this.deviceDeatils = stringify;

      this._io = io.connect(this.urls.mainURL + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
      this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
      this.showShareBtn = true;
      // var paramData = this.navParams.get("device");
      this.titleText = this.deviceDeatils.Device_Name;
      if (that.allData.map != undefined) {
        that.allData.map.remove();
      }
      this.temp(this.deviceDeatils);
      this.showActionSheet = true;
    }
  }

}


