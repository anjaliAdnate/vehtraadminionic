import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCustomerModal } from './add-customer-modal';

@NgModule({
  declarations: [
    AddCustomerModal
  ],
  imports: [
    IonicPageModule.forChild(AddCustomerModal)
  ]
})
export class AddCustomerModalModule {}